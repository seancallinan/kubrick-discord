FROM mhart/alpine-node:6
RUN apk add --no-cache make gcc g++ python git tzdata && cp /usr/share/zoneinfo/Pacific/Auckland /etc/localtime && echo "Pacific/Auckland" > /etc/timezone && date
ADD package.json /tmp/package.json
RUN cd /tmp && npm install && mkdir -p /app && mv /tmp/node_modules /app/node_modules \
    && apk del make gcc g++ python git tzdata && rm -rf /tmp/* /root/.npm /root/.node-gyp
VOLUME /persist.db
WORKDIR /app
ADD . .
CMD npm run bot
