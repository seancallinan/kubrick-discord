module.exports = {

  // Your bot name. Typically, this is your bot's username without the discriminator.
  // i.e: if your bot's username is MemeBot#0420, then this option would be MemeBot.
  name: process.env['BOT_NAME'] || '',

  // The bot's command prefix. The bot will recognize as command any message that begins with it.
  // i.e: "-kubrick foo" will trigger the command "foo",
  //      whereas "Kubrick foo" will do nothing at all.
  prefix:  process.env['BOT_PREFIX'] || '!k',
    dbPath: process.env['DB_PATH'] || './test.db',
    dbPollInterval: 5000,

  // Your bot's user token. If you don't know what that is, go here:
  // https://discordapp.com/developers/applications/me
  // Then create a new application and grab your token.
  token: process.env['BOT_TOKEN'],

  // If this option is enabled, the bot will delete the message that triggered it, and its own
  // response, after the specified amount of time has passed.
  // Enable this if you don't want your channel to be flooded with bot messages.
  // ATTENTION! In order for this to work, you need to give your bot the following permission:
  // MANAGE_MESSAGES - 	0x00002000
  // More info: https://discordapp.com/developers/docs/topics/permissions
  deleteAfterReply: {
    enabled: false,
    time: 10000, // In milliseconds
  },
    defaultTz: 'Pacific/Auckland',
  timezones: [
      {
        name: 'New Zealand',
        aliases: ['NZ', 'NZT', 'NZDT'],
        iana: 'Pacific/Auckland',
          gmtOffset: '+1300'
      },
    {
      name: 'Brisbane',
      aliases: ['QLD', 'AEST'],
      iana: 'Australia/Brisbane',
        gmtOffset: '+1000'
    },
    {
      name: 'Sydney',
      aliases: ['NSW', 'AEDT', 'SYD'],
        iana: 'Australia/Sydney',
        gmtOffset: '+1100'
    },
      {
          name: 'Melbourne',
          aliases: ['VIC', 'AEDT', 'MEL'],
          iana: 'Australia/Melbourne',
          gmtOffset: '+1100'
      },
    {
      name: 'Adelaide',
      aliases: ['SA', 'ACDT'],
      iana: 'Australia/Adelaide',
        gmtOffset: '+1030'
    },
    {
      name: 'Darwin',
      aliases: ['NT', 'ACST'],
      iana: 'Australia/Darwin',
        gmtOffset: '+0930'
    },
    {
      name: 'Perth',
      aliases: ['WA', 'AWST'],
      iana: 'Australia/Perth',
        gmtOffset: '+0800'
    },
	{
	name: 'Tahiti/Cook Islands',
	aliases: ['Tahiti', 'Cook Islands', 'Raro'],
	iana: 'Pacific/Tahiti',
	gmtOffset: '-1000'
},
      {
          name: 'UTC',
          aliases: ['GMT'],
          iana: 'Etc/UTC',
          gmtOffset: '+0000'
      }
  ]
};
