const sqlite3 = require('sqlite3');
const fs      = require('fs');

module.exports = (() => {

    var db;
    var poller;

    var cache = {};
    var queries = [];

    var connect = (dbPath) => {
        db = new sqlite3.Database(dbPath);
        fs.readdirSync('./lib/modules/persist/queries').forEach(file => {
            queries.push(require('./queries/' + file));
        });
        console.log('[PERSIST]', 'connected to', dbPath);
    };

    var beginPolling = (interval) => {
        if(!db) return console.log('[PERSIST]', 'not connected, use connect() first');
        poller = setInterval(() => {
            var promises = queries.map(q => q(db));
            Promise.all(promises).then((results) => {
                results.forEach(result => {
                    cache[result.name] = result.results;
                });

            }).catch((err) => {
                console.error('error during poll', err);
            });
        }, interval);
        console.log('[PERSIST]', 'beginning poll every', interval + 'ms');
    };

    var stopPolling = () => {
        if(poller) clearInterval(poller);
        console.log('[PERSIST]', 'Stopping poll');
    };

    var disconnect = () => {
        console.log('[PERSIST]', 'disconnecting');
        db.close();
        db = null;
    };

    var query = (query, params) => {
        if(!db) return console.log('[PERSIST]', 'not connected, use connect() first');
        db.serialize(() => {
            var stmt = db.prepare(query);
            params.forEach(p => {
                stmt.run(p);
            });
            stmt.finalize();
        });
    };

    return {
        connect: connect,
        results: cache,
        beginPolling: beginPolling,
        stopPolling: stopPolling,
        disconnect: disconnect,
        query: query
    }
})();
