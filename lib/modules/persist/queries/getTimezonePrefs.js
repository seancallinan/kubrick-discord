var queryName = "getTimezonePrefs";
module.exports = (db) => {
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.all('select user_id, timezone from timezone_prefs', (err, results) => {
                if(err) return reject(err);
                return resolve({name: queryName, results: results});
            });
        });
    });
};
