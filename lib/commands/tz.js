var Clapp = require('../modules/clapp-discord');
const moment = require('moment-timezone'), chrono = require('chrono-node');
var cfg = require('../../config');

const getTimezoneHelp = () => {
    var message = '';
    cfg.timezones.forEach(e => {
        if(message != '') message += ', '
        message += e.name + ' (aliases: ' + e.aliases.join(', ') + ', ' + e.iana + ')'
    });
    return message;
};

const getDefaultTz = (ctx) => {
    var userid = ctx.msg.author.id;
    if(ctx.persist.results.hasOwnProperty('getTimezonePrefs')){
        var prefs = ctx.persist.results.getTimezonePrefs.find(a => {return a.user_id == userid});
        if(prefs){
            return prefs.timezone;
        }
    }
    return cfg.defaultTz;
};

module.exports =

    new Clapp.Command({
        name: "tz",
        desc: "Gets a set of timezone conversions for a given time and timezone",
        fn: (argv, context) => {
            // This output will be redirected to your app's onReply function

            var timeInput = argv.args.time;
            var timezoneInput = (argv.flags.timezone !== 'default')? argv.flags.timezone : getDefaultTz(context);

            var timezone = cfg.timezones.find((e) => {
                // check if input matches a tz name
                if (timezoneInput.toUpperCase() == e.name.toUpperCase()) return true;
                // or any of its aliases
                if (e.aliases.find(q => {return q.toUpperCase() == timezoneInput.toUpperCase()})) return true;
                // or its IANA name, else return false.
                return timezoneInput.toUpperCase() == e.iana.toUpperCase();
            });

            if (!timezone) {
                return 'I don\'t recognise the timezone you\'ve provided, valid timezones are ' + getTimezoneHelp();
            }

            var parsedTime = chrono.parseDate(timeInput + ' GMT' + timezone.gmtOffset);

            var momentObject = moment(parsedTime).tz(timezone.iana);

            var message = '';

            message += momentObject.format('dddd, MMMM Do YYYY, HH:mm') + '\nTimezone: ' + timezone.name;

            message += '\n\nIn other timezones: \n';

            var otherTimezones = cfg.timezones.filter(a => {return a.iana !== timezone.iana});

            otherTimezones.forEach(t => {
                var p = moment(momentObject).tz(t.iana);
                message += '\n' + t.name + ': ' + p.format('ddd, HH:mm');
            });


            return message;
        },
        args: [
            {
                name: 'time',
                desc: 'The time to convert',
                type: 'string',
                required: true
            }

        ],
        flags: [
            {
                name: 'timezone',
                desc: 'Timezone for time provided, if omitted will use default',
                type: 'string',
                alias: 'z',
                default: 'default'
            }
        ]
    });
