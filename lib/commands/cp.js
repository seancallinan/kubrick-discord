var Clapp = require('../modules/clapp-discord');
const moment = require('moment-timezone'), chrono = require('chrono-node');
var cfg = require('../../config');

const getDefaultTz = (ctx) => {
    var userid = ctx.msg.author.id;
    if(ctx.persist.results.hasOwnProperty('getTimezonePrefs')){
        var prefs = ctx.persist.results.getTimezonePrefs.find(a => {return a.user_id == userid});
        if(prefs){
            return prefs.timezone;
        }
    }
    return cfg.defaultTz;
};

const calculateCheckpoints = (first, count, tz) => {

    var mEnd = moment(new Date(first)).tz(tz);
    var times = [mEnd];
    for(var i = 1; i<count; i++){
        var m = moment(mEnd).clone().tz(tz);
        times.push(m.add(i*5, 'hours'));
    }

    return times;
};

const CHECKPOINT = 5*60*60, CYCLE = 7*25*60*60;

module.exports =

    new Clapp.Command({
        name: "cp",
        desc: "Gets the next 5 septicycle checkpoints in different timezones",
        fn: (argv, context) => {
            // This output will be redirected to your app's onReply function

            var timezoneInput = getDefaultTz(context);

            var timezone = cfg.timezones.find((e) => {
                // check if input matches a tz name
                if (timezoneInput.toUpperCase() == e.name.toUpperCase()) return true;
                // or any of its aliases
                if (e.aliases.find(q => {return q.toUpperCase() == timezoneInput.toUpperCase()})) return true;
                // or its IANA name, else return false.
                return timezoneInput.toUpperCase() == e.iana.toUpperCase();
            });

            var now = new Date().getTime() + (argv.flags.skip * CHECKPOINT * 1000);

            var checkpointStart = Math.floor(now / (CHECKPOINT*1000)) * (CHECKPOINT*1000);
            var checkpointEnd = checkpointStart + CHECKPOINT*1000;



            var checkpoints = calculateCheckpoints(checkpointEnd, 5, timezone.iana);


            var message = '';

            message += 'Next five' + ((argv.flags.skip !== 0)? ' (+ ' + argv.flags.skip + ' skipped)':'') + ' checkpoints: ' + checkpoints.map(a => {return moment(a).format('ddd HH:mm')}).join(', ') + '\nTimezone: ' + timezone.name;

            message += '\n\nIn other timezones: \n';

            var otherTimezones = cfg.timezones.filter(a => {return a.iana !== timezone.iana});

            otherTimezones.forEach(t => {
                var cps = calculateCheckpoints(checkpointEnd, 5, t.iana);

                message += '\n' + t.name + ': ' + cps.map(a => {return moment(a).format('ddd HH:mm')}).join(', ');
            });


            return message;
        },
        args: [


        ],
        flags: [
		{
                name: 'skip',
                desc: 'Skip a number of checkpoints',
                type: 'number',
                alias: 's',
                default: 0
            }

        ]
    });
