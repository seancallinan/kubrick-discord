var Clapp = require('../modules/clapp-discord');
const moment = require('moment-timezone'), chrono = require('chrono-node');
var cfg = require('../../config');

const getTimezoneHelp = () => {
    var message = '';
    cfg.timezones.forEach(e => {
        if(message != '') message += ', '
        message += e.name + ' (aliases: ' + e.aliases.join(', ') + ', ' + e.iana + ')'
    });
    return message;
};

module.exports =

    new Clapp.Command({
        name: "tzset",
        desc: "Sets the default timezone for a user",
        fn: (argv, context) => {
            // This output will be redirected to your app's onReply function

            var timezoneInput = argv.args.timezone;

            var timezone = cfg.timezones.find((e) => {
                // check if input matches a tz name
                if (timezoneInput.toUpperCase() == e.name.toUpperCase()) return true;
                // or any of its aliases
                if (e.aliases.find(q => {return q.toUpperCase() == timezoneInput.toUpperCase()})) return true;
                // or its IANA name, else return false.
                return timezoneInput.toUpperCase() == e.iana.toUpperCase();
            });

            if (!timezone) {
                return 'Whoops, I don\'t recognise the timezone you\'ve provided, valid timezones are ' + getTimezoneHelp();
            }

            var userid = context.msg.author.id;

            context.persist.query('replace into timezone_prefs (user_id, timezone) values (?, ?)', [[userid, timezone.iana]]);

            var message = 'I have updated your default timezone to ' + timezone.name;


            return message;
        },
        args: [
            {
                name: 'timezone',
                desc: 'The timezone for the user',
                type: 'string',
                required: true
            }

        ]
    });
