var Clapp = require('../modules/clapp-discord');

module.exports = new Clapp.Command({
  name: "ping",
  desc: "Checks to see if I'm alive",
  fn: (argv, context) => {
    // This output will be redirected to your app's onReply function
	return 'pong!';
  },
  args: [
  ],
  flags: [
  ]
});
