#!/bin/bash
source .creds
docker build -t seancallinan/kubrick .
docker rm -f kubrick
docker run --restart=always -d -v /tmp/kubrick.db:/persist.db \
    --name kubrick -e BOT_TOKEN=$BOT_TOKEN -e BOT_NAME=$BOT_NAME -e DB_PATH=/persist.db -t seancallinan/kubrick