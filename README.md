![logo][logo]
# Kubrick - A Clockwork Discord #

This is a bot that has some useful features for querying things to do with time.

### Current features ###

* Converts time from timezones into others
* Stores a default timezone for users
* Retrieves the next 5 septicycle checkpoints (Ingress) in different timezones

### How do I get set up? ###

* Build docker image
* Register for a bot user on Discord
* Add the bot user to your server
* Run a docker container for the image, passing in env vars: BOT_TOKEN and BOT_NAME with the token and name respectively


[logo]: https://i.imgur.com/hRyw9Me.png
